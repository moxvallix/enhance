# Kirigami/QML Image Upscaler

![Batch Screen](https://i.imgur.com/3Z4NUlG.jpg)

This is a WIP, batch image upscaler using [Real-ESRGAN-ncnn-vulkan](https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan).
For now, I have gone with the name "Enhance!", but I have received suggestions to name it "Upskaler".

## Setup
To run this project, you need a version of [Ruby](https://www.ruby-lang.org/), preferably 3.1.0 or higher.
This project uses [ruby-qml](https://github.com/seanchas116/ruby-qml) to provide Ruby QML bindings.

Then, in the base directory of this project, you will want to run `bundle install`. This should install
the required gems automatically.

Next you will need to download the models and binary. You can find them [here](https://github.com/xinntao/Real-ESRGAN/releases/download/v0.2.5.0/realesrgan-ncnn-vulkan-20220424-ubuntu.zip).

Extract the `models/` folder, and `realesrgan-ncnn-vulkan`, from the zip, into the `bin/` directory.
Make sure to mark `realesrgan-ncnn-vulkan` as executable.

To start the project, `ruby bin/start.rb`

## Usage
Files can be dragged into the window, or imported with the file picker.

To upscale the imported images, click "Let's Enhance!" and select the output folder.

After finishing a batch, it currently does not return to the main screen.
Dragging a new file into the window will make it do so.
