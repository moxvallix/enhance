module Enhance
  class Base
    require_rel "."

    def config
      @config ||= Config.new
    end

    def cache
      @cache ||= get_cache_folder
    end

    def temp
      @temp ||= get_temp_folder
    end

    def logger
      @logger ||= Logger.new(config.log)
    end

    private

    def get_cache_folder
      xdg_cache = XDG::Cache.new
      app_cache = xdg_cache.home.join("enhance")
      app_cache.mkpath
      app_cache
    end

    def get_temp_folder
      temp_dir = cache.join("tmp")
      temp_dir.mkpath
      temp_dir
    end
  end
end