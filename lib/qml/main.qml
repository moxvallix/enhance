import QtQuick 2.15
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.3
import org.kde.kirigami 2.13 as Kirigami
import Enhance 0.1

// Base element, provides basic features needed for all kirigami applications
Kirigami.ApplicationWindow {
    // ID provides unique identifier to reference this element
    id: root

    // Window title
    // i18nc is useful for adding context for translators, also lets strings be changed for different languages
    title: "Enhance!"

    // Initial page to be loaded on app load
    pageStack.initialPage: Qt.resolvedUrl("start.qml")

    Timer {
        property var item
        id: killTimer
        interval: 50
        onTriggered: {
            console.debug("Destroying " + item)
            item.destroy()
            enhanceQueue.quit()
        }

        function kill(item) {
            killTimer.item = item
            killTimer.restart()
        }
    }

    onClosing: {
        killTimer.kill(this)
        close.accepted = false
    }

    function addToQueue(values) {
        enhanceQueue.add = Array.from(values)
    }

    function switchToPage(page) {
        applicationWindow().pageStack.clear()
        applicationWindow().pageStack.push(Qt.resolvedUrl(page))
    }

    Enhance {
        id: enhanceQueue
        onShowGrid: switchToPage("grid.qml")
        onShowStart: switchToPage("start.qml")
    }

    FileDialog {
        id: fileDialog
        selectMultiple: true
        nameFilters: ["Image Files (*.png *.jpg *.webp)"]
        onAccepted: {
            addToQueue(fileUrls)
        }
    }

    FileDialog {
        id: folderDialog
        selectFolder: true
        title: "Select Output Folder"
        onAccepted: {
            switchToPage("process.qml")
            enhanceQueue.outputFolder = String(folder)
        }
    }


    DropArea {
        anchors.fill: parent
        onDropped: {
            addToQueue(drop.urls)
        }
    }

}
