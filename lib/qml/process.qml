import QtQuick 2.15
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.3
import org.kde.kirigami 2.13 as Kirigami
import Enhance 0.1

Kirigami.ScrollablePage {
    Connections {
        target: enhanceQueue
        function onUpdateProgress(index, total) {
            enhanceProgress.value = index
            enhanceProgress.to = total
        }
    }

    Kirigami.CardsListView {
        id: enhanceProcessQueue
        model: enhanceQueue.model
        delegate:  Kirigami.AbstractCard  {
            id: card
            contentItem: Item {
                implicitWidth: delegateLayout.implicitWidth
                implicitHeight: delegateLayout.implicitHeight
                GridLayout {
                    id: delegateLayout
                    anchors {
                        left: parent.left
                        top: parent.top
                        right: parent.right
                        //IMPORTANT: never put the bottom margin
                    }
                    rowSpacing: Kirigami.Units.largeSpacing
                    columnSpacing: Kirigami.Units.largeSpacing
                    columns: width > Kirigami.Units.gridUnit * 20 ? 4 : 2
                    Kirigami.Icon {
                        source: path
                        Layout.fillHeight: true
                        Layout.maximumHeight: Kirigami.Units.iconSizes.huge
                        Layout.preferredWidth: height
                    }
                    Kirigami.Heading {
                        Layout.fillWidth: true
                        level: 2
                        text: title
                    }
                }
            }
        }
    }
    footer: Controls.ProgressBar {
        id: enhanceProgress
        from: 0
        to: 100
        value: 0
        indeterminate: false
    }
}