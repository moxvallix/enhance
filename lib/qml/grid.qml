import QtQuick 2.15
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.3
import org.kde.kirigami 2.13 as Kirigami
import Enhance 0.1

Kirigami.ScrollablePage {
    actions {
        main: Kirigami.Action {
            text: "Configure"
            iconName: "xapp-prefs-behavior-symbolic"
            onTriggered: configSheet.open()
        }
        contextualActions: [
            Kirigami.Action {
                text: "Import Files"
                iconName: "folder-open"
                onTriggered: fileDialog.open()
            },
            Kirigami.Action {
                text: "Let's Enhance"
                iconName: "xapp-prefs-display-symbolic"
                onTriggered: folderDialog.open()
            }
        ]
    }

    Kirigami.OverlaySheet {
        id: configSheet
        Controls.Label {
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: "Weeeeee, overlay sheet!"
        }
    }

    Item {
        height: childrenRect.height
        GridView {
            anchors.horizontalCenter: parent.horizontalCenter
            id: enhanceGrid
            model: enhanceQueue.model
            property int cardSize: 220
            property int outerPadding: 10
            property int cardRowSize: Math.floor((parent.width - outerPadding) / cardSize)
            property int calcHeight: Math.floor(count / cardRowSize) * cardSize
            cellHeight: cardSize
            cellWidth: cardSize
            width: cardRowSize * cardSize
            height: calcHeight
            delegate:  Kirigami.Card  {
                id: card
                width: 200; height: 200
                banner {
                    title: title
                    source: path
                    sourceSize.width: 200
                    sourceSize.height: 200
                    fillMode: Image.PreserveAspectCrop
                }
                actions: [
                    Kirigami.Action {
                        text: "Remove"
                        icon.name: "trashcan_empty"
                        onTriggered: {
                            enhanceQueue.remove = path
                        }
                    }
                ]
            }
        }
    }
}